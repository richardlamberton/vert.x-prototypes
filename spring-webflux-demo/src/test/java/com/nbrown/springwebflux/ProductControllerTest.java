package com.nbrown.springwebflux;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import com.nbrown.springwebflux.controller.ProductController;
import com.nbrown.springwebflux.model.Product;
import com.nbrown.springwebflux.service.ProductService;

import reactor.core.publisher.Flux;

@RunWith(SpringRunner.class)
@WebFluxTest(ProductController.class)
public class ProductControllerTest {

	@Autowired
	private WebTestClient webTestClient;
	
	@MockBean
    private ProductService productService;
	
	@Test
	public void givenProductWhenCreateThenCreated() throws Exception {
		String testProduct = "{" +
				  "\"productId\"   : 1," +
				  "\"description\" : \"Longline dress\"," +
				  "\"price\"       : 19.99," +
				  "\"imageUrl\"    : \"https://localhost:9002/dress.jpg\"," +
				  "\"inSale\"      : true" +
				"}";
		
		webTestClient.post()
		    .uri("/products")
		    .contentType(MediaType.APPLICATION_JSON)
		    .body(BodyInserters.fromObject(testProduct))
		    .exchange()
		    .expectStatus().isCreated();
		
		verify(productService).create(any(Product.class));
	}
	
	@Test
	public void givenInvalidProductWhenCreateThenBadRequest() throws Exception {
		String invalidJson = "INVALID JSON";
		
		webTestClient.post()
		    .uri("/products")
		    .contentType(MediaType.APPLICATION_JSON)
		    .body(BodyInserters.fromObject(invalidJson))
		    .exchange()
		    .expectStatus().isBadRequest();
		
		verify(productService, never()).create(null);
	}
	
	@Test
	public void givenProductsWhenFindAllThenMultipleProductsReturned() throws Exception {
		BDDMockito.given(productService.findAll()).willReturn(givenProductsAsFlux());
		
		List<Product> results = webTestClient.get()
		    .uri("/products")
		    .exchange()
		    .expectStatus().isOk()
		    .expectBodyList(Product.class)
		    .hasSize(2)
		    .returnResult().getResponseBody();
		
		verify(productService).findAll();
		assertEquals(1, results.get(0).getProductId());
		assertEquals(10.99, results.get(0).getPrice(), 0);
		assertEquals(2, results.get(1).getProductId());
		assertEquals(15.01, results.get(1).getPrice(), 0);
	}

	private Flux<Product> givenProductsAsFlux() {
		Product[] products = new Product[2];
		
		Product p1 = new Product();
		p1.setDescription("Black dress");
		p1.setImageUrl("http://localhost/image1.jpg");
		p1.setInSale(true);
		p1.setPrice(10.99);
		p1.setProductId(1);

		Product p2 = new Product();
		p2.setDescription("Green dress");
		p2.setImageUrl("http://localhost/image2.jpg");
		p2.setInSale(false);
		p2.setPrice(15.01);
		p2.setProductId(2);

		products[0] = p1;
		products[1] = p2;
		
		Flux<Product> productsFlux = Flux.fromArray(products);
		return productsFlux;
	}
}
