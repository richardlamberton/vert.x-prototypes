package com.nbrown.vertxdynamodb.dao;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AmazonDynamoDBException;
import com.nbrown.vertxdynamodb.model.Product;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.Json;

public class DynamodbProductDaoVerticle extends AbstractVerticle {

	private AmazonDynamoDBAsync dynamoClient;

	private static final String TABLE_NAME = "products";

	public DynamodbProductDaoVerticle() {
		System.out.println("Connecting to DynamoDB...");
		EndpointConfiguration endpointConfiguration = new EndpointConfiguration("http://localhost:8000", "EU");
		
		dynamoClient = AmazonDynamoDBAsyncClientBuilder.standard()
                .withEndpointConfiguration(endpointConfiguration)
                .withCredentials(new ProfileCredentialsProvider("myProfile"))
                .build();
		System.out.println("Connected");
	}
	
	@Override
	public void start(Future<Void> future) {
		vertx.eventBus().consumer("dynamodb.insert.product", message -> {

			Boolean result = false;
			try {
				// message.body contains the JSON supplied in the HTTP POST
				Product product = Json.decodeValue(message.body().toString(), Product.class);
			    result = insert(product);
			    if (!result) {
			    	message.fail(123, "Failed to insert the product " + product.getProductId());
			    }
			} catch (Exception ex) {
				message.fail(124,  "Unexpected exception: " + ex.getMessage());
			}

		    message.reply(result);
		});
	}
	
	
	
	/**
	 * Insert a product
	 */
	private Boolean insert(Product product) {
		boolean result = false;

		DynamoDB dynamoDB = new DynamoDB(dynamoClient);
		Table table = dynamoDB.getTable(TABLE_NAME);
		
		Item productItem = new Item()
				.withPrimaryKey("productId", product.getProductId())
				.withString("description", product.getDescription())
				.withString("imageUrl", product.getImageUrl())
				.withDouble("price", product.getPrice());
		
		try {
			PutItemOutcome outcome = table.putItem(productItem);
			result = true;
		} catch (AmazonDynamoDBException ex) {
			// Failed - Log an error
		}

		return result;
	}

}
