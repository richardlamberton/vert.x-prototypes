package com.nbrown.vertxdynamodb.verticle;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nbrown.vertxdynamodb.model.Product;
import com.nbrown.vertxdynamodb.service.IProductService;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

/**
 * Example of using Vert.x to provide asynchronous access to a service/mongoDB layer
 */
public class ProductVerticle extends AbstractVerticle {

	private IProductService productService;

	@Override
	public void start(Future<Void> future) {
		Router router = Router.router(vertx);

		router.route().handler(BodyHandler.create());

		router.post("/products").handler(this::createProduct);
		router.get("/products/:productId").handler(this::getProduct);

		vertx.createHttpServer().requestHandler(router::accept).listen(config().getInteger("http.port", 8080),
				result -> {
					if (result.succeeded()) {
						future.complete();
					} else {
						future.fail(result.cause());
					}
				});
	}

	/**
	 * Create/replace product
	 */
	public void createProduct(RoutingContext routingContext) {
		JsonObject body = routingContext.getBodyAsJson();
		
		Product product = Json.decodeValue(body.encode(), Product.class);
		
//		vertx.executeBlocking(blockingCodeHandler, resultHandler);
		vertx.executeBlocking(future -> {
		   try {
			   productService.create(product);
			   future.complete();
		   } catch (Exception ex) {
		       future.fail(ex);
		   }
		}, res -> {
			if (res.succeeded()) {
				routingContext.response()
			    .setStatusCode(201)
			    .putHeader("content-type", "text/plain")
			    .end("OK");
			} else {
				serviceUnavailable(routingContext);
			}
		});

	}

	/**
	 * Get single product
	 */
	public void getProduct(RoutingContext routingContext) {
		String productId = routingContext.request().getParam("productId");
		if (productId == null) {
			badRequest(routingContext);
			
		} else {
			
			Product product = productService.findById(Integer.valueOf(productId));
		
			if (product == null) {
				notFound(routingContext);
			} else {
				try {
					routingContext.response()
						.putHeader("content-type", "application/json")
						.end(new ObjectMapper().writeValueAsString(product));
				} catch (JsonProcessingException e) {
					serviceUnavailable(routingContext);
				}
			}
		}
	}

	
	
	private void sendError(int statusCode, HttpServerResponse response) {
		response.setStatusCode(statusCode).end();
	}

	private void badRequest(RoutingContext context) {
		context.response().setStatusCode(400).end();
	}

	private void notFound(RoutingContext context) {
		context.response().setStatusCode(404).end();
	}

	private void serviceUnavailable(RoutingContext context) {
		context.response().setStatusCode(503).end();
	}

	
	public void setProductService(IProductService productService) {
		this.productService = productService;
	}

}
