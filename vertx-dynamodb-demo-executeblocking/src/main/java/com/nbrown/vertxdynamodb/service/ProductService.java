package com.nbrown.vertxdynamodb.service;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClientBuilder;
import com.nbrown.vertxdynamodb.dao.DynamodbProductDao;
import com.nbrown.vertxdynamodb.model.Product;

import io.vertx.core.Vertx;

public class ProductService implements IProductService {
	
    private DynamodbProductDao productDao;

    /**
     * Provides asynchronous access to a Dynamo database, using asynchronous driver
     */
    public ProductService(Vertx vertx) {
		System.out.println("Connecting to DynamoDB...");
		EndpointConfiguration endpointConfiguration = new EndpointConfiguration("http://localhost:8000", "EU");
		
		AmazonDynamoDBAsync dynamoClient;

		dynamoClient = AmazonDynamoDBAsyncClientBuilder.standard()
                .withEndpointConfiguration(endpointConfiguration)
                .withCredentials(new ProfileCredentialsProvider("myProfile"))
                .build();
		System.out.println("Connected");

		productDao = new DynamodbProductDao(dynamoClient);
    }
    
    public Boolean create(Product e) {
        return productDao.insert(e);
    }

    public Product findById(Integer id) {
        return productDao.findById(id);
    }

//    public Future<Collection<Product>> findAll() {
//        return productDao.findAll();
//    }
//
//    public Future<Boolean> delete(Integer id) {
//        return productDao.deleteById(id);
//    }

}
