package com.nbrown.vertxdynamodb.service;

import com.nbrown.vertxdynamodb.model.Product;

public interface IProductService {
    Boolean create(Product e);
     
    Product findById(Integer id);

//    Future<Collection<Product>> findAll();
//
//    Future<Boolean> delete(Integer id);
}