package com.nbrown.vertxdynamodb;

import com.nbrown.vertxdynamodb.service.ProductService;
import com.nbrown.vertxdynamodb.verticle.ProductVerticle;

import io.vertx.core.Vertx;

public class VertxDynamoDBApp {
    public static void main( String[] args ) {
    	Vertx vertx = Vertx.vertx();
    	ProductVerticle productVerticle = new ProductVerticle();
        vertx.deployVerticle(productVerticle);
        
		ProductService productService = new ProductService(vertx);
		productVerticle.setProductService(productService);
    }
}
