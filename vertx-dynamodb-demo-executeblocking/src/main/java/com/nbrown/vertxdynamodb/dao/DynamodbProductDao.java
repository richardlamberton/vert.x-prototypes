package com.nbrown.vertxdynamodb.dao;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AmazonDynamoDBException;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.nbrown.vertxdynamodb.model.Product;


/**
 * Example of asynchronous access to a mongo database, by using Future wrappers for the response
 */
public class DynamodbProductDao {

	private AmazonDynamoDBAsync dynamoClient;

	private static final String TABLE_NAME = "products";
	
	public DynamodbProductDao(AmazonDynamoDBAsync dynamoClient) {
		this.dynamoClient = dynamoClient;
	}

	/**
	 * Insert a product
	 */
	public Boolean insert(Product product) {
		boolean result = false;

		DynamoDB dynamoDB = new DynamoDB(dynamoClient);
		Table table = dynamoDB.getTable("Products");
		
		Item productItem = new Item()
				.withPrimaryKey("productId", product.getProductId())
				.withString("description", product.getDescription())
				.withString("imageUrl", product.getImageUrl())
				.withDouble("price", product.getPrice());
		
		try {
			PutItemOutcome outcome = table.putItem(productItem);
			result = true;
		} catch (AmazonDynamoDBException ex) {
			// Failed - Log an error
		}

		return result;
	}

	/**
	 * Find a product by its productId
	 */
	public Product findById(Integer productId) {
		Product product = null;
		
		Map<String, AttributeValue> key = new HashMap<>();
		AttributeValue val = new AttributeValue();
		val.setN(String.valueOf(productId));
		key.put("productId", val);
		
		try {
			GetItemResult result = dynamoClient.getItem(
				new GetItemRequest().withTableName(TABLE_NAME).withKey(key));
		
			if ((result.getItem() != null) && (!result.getItem().isEmpty())) {
				product = new Product();
				product.setProductId(Integer.parseInt(result.getItem().get("productId").getN()));
				product.setDescription(result.getItem().get("description").getS());
				product.setImageUrl(result.getItem().get("imageUrl").getS());
				product.setPrice(Double.parseDouble(result.getItem().get("price").getN()));
				product.setInSale(result.getItem().get("inSale").getBOOL());
			} else {
				// Item not found
			}
			
		} catch (AmazonDynamoDBException ex) {
			// Log an error
		}
		
		return product;
	}

//	/**
//	 * Find all products
//	 */
//	public Future<Collection<Product>> findAll() {
//		Future<Collection<Product>> result = Future.future();
//
//		JsonObject query = new JsonObject();
//		
//		FindOptions options = new FindOptions().setSort(new JsonObject().put("productId", 1));
//
//		dynamoClient.findWithOptions(DB_NAME, query, options, res -> {
//            if (res.succeeded()) {
//            	List<Product> products = new ArrayList<>();
//            	for (JsonObject json : res.result()) {
//            		products.add(Json.decodeValue(json.encode(), Product.class));
//            	}
//		        System.out.println("Retrieved " + products.size() + " products");
//                result.complete(products);
//            } else {
//            	result.fail(res.cause());
//            }
//        });
//	
//		return result;
//	}
//
//	/**
//	 * Remove a product by its productId
//	 */
//	public Future<Boolean> deleteById(Integer productId) {
//		Future<Boolean> result = Future.future();
//
//		JsonObject query = new JsonObject()
//				  .put("productId", productId);
//		
//		dynamoClient.removeDocument(DB_NAME, query, res -> {
//            if (res.succeeded() && res.result().getRemovedCount() > 0) {
//		        System.out.println("Removed " + res.result().getRemovedCount() + " product(s)");
//                result.complete(true);
//            } else {
//            	result.fail("failed to delete product");
//            }
//        });
//		
//		return result;
//	}

}
