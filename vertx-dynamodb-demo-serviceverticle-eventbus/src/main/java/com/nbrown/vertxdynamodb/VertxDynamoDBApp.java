package com.nbrown.vertxdynamodb;

import com.nbrown.vertxdynamodb.controller.ProductVerticle;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;

public class VertxDynamoDBApp {
    public static void main( String[] args ) {
    	Vertx vertx = Vertx.vertx();

    	// Product Verticle
    	ProductVerticle productVerticle = new ProductVerticle();
        vertx.deployVerticle(productVerticle);

        // Database Verticle
        DeploymentOptions options = new DeploymentOptions()
        		  .setWorker(true)
        		  .setInstances(5);
		vertx.deployVerticle("com.nbrown.vertxdynamodb.service.ProductService", options);
    }
}
