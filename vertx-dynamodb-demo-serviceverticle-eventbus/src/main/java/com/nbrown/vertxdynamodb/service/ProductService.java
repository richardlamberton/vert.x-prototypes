package com.nbrown.vertxdynamodb.service;

import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.nbrown.vertxdynamodb.dao.ProductDao;
import com.nbrown.vertxdynamodb.model.Product;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.Json;

public class ProductService extends AbstractVerticle {

	private ProductDao productDao;

	private boolean async = true;
	
	public ProductService() {
		productDao = new ProductDao();
	}
	
	@Override
	public void start(Future<Void> future) {
		vertx.eventBus().consumer("create.product", message -> {
			if (async) {
				createProductAsync(message);
			} else {
				createProduct(message);
			}
		});
	}

	private void createProduct(Message<Object> message) {
		Boolean result = false;
		try {
			// message.body contains the JSON supplied in the HTTP POST
			Product product = Json.decodeValue(message.body().toString(), Product.class);
		    result = productDao.insert(product);
		    if (!result) {
		    	message.fail(123, "Failed to insert the product " + product.getProductId());
		    }
		} catch (Exception ex) {
			message.fail(124,  "Unexpected exception: " + ex.getMessage());
		}

	    message.reply(result);
	}
	
	private void createProductAsync(Message<Object> message) {
		Boolean result = false;
		try {
			// message.body contains the JSON supplied in the HTTP POST
			Product product = Json.decodeValue(message.body().toString(), Product.class);
			
			java.util.concurrent.Future<PutItemResult> future = productDao.insertAsync(product);
			if (future.isDone()) {
				message.reply(true);
			} else {
				message.fail(123,  "Failed to insert the product : " + product.getProductId());
			}
			
		} catch (Exception ex) {
			message.fail(124,  "Unexpected exception: " + ex.getMessage());
		}

	    message.reply(result);
	}
	
	
}
