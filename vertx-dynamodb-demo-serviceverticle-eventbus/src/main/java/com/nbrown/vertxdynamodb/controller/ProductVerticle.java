package com.nbrown.vertxdynamodb.controller;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

/**
 * Example of using Vert.x to provide asynchronous access to a service/mongoDB layer
 */
public class ProductVerticle extends AbstractVerticle {

	@Override
	public void start(Future<Void> future) {
		Router router = Router.router(vertx);

		router.route().handler(BodyHandler.create());

		router.post("/products").handler(this::createProduct);
//		router.get("/products").handler(this::getAllProducts);
//		router.get("/products/:productId").handler(this::getProduct);
//		router.delete("/products/:productId").handler(this::deleteProduct);

		vertx.createHttpServer().requestHandler(router::accept).listen(config().getInteger("http.port", 8080),
				result -> {
					if (result.succeeded()) {
						future.complete();
					} else {
						future.fail(result.cause());
					}
				});
	}

	/**
	 * Create/replace product
	 */
	public void createProduct(RoutingContext routingContext) {
		// Invoke the service verticle by sending a msg over the event bus, and await a reply
		vertx.eventBus().send("create.product", routingContext.getBodyAsString(),
				res -> {
					if (res.succeeded()) {
						routingContext.response()
					    .setStatusCode(201)
					    .putHeader("content-type", "text/plain")
					    .end("OK");
					} else {
						System.out.println(res.cause());
						serviceUnavailable(routingContext);
					}
				});
		}


	
	
	private void sendError(int statusCode, HttpServerResponse response) {
		response.setStatusCode(statusCode).end();
	}

	private void badRequest(RoutingContext context) {
		context.response().setStatusCode(400).end();
	}

	private void notFound(RoutingContext context) {
		context.response().setStatusCode(404).end();
	}

	private void serviceUnavailable(RoutingContext context) {
		context.response().setStatusCode(503).end();
	}


}
