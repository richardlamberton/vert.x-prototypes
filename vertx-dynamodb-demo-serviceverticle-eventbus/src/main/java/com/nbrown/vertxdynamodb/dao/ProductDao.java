package com.nbrown.vertxdynamodb.dao;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemUtils;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AmazonDynamoDBException;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.nbrown.vertxdynamodb.model.Product;

public class ProductDao {

	private AmazonDynamoDBAsync dynamoClient;

	private static final String TABLE_NAME = "products";

	public ProductDao() {
		System.out.println("Connecting to DynamoDB...");
		EndpointConfiguration endpointConfiguration = new EndpointConfiguration("http://localhost:8000", "EU");
		
		dynamoClient = AmazonDynamoDBAsyncClientBuilder.standard()
                .withEndpointConfiguration(endpointConfiguration)
                .build();
		System.out.println("Connected");
	}
	
	/**
	 * Insert a product
	 */
	public Boolean insert(Product product) {
		boolean result = false;

		// NOTE: TABLE.PUTITEM is a synchronous call. Need to use the putItemAsync method on the dynamoClient instead
		DynamoDB dynamoDB = new DynamoDB(dynamoClient);
		Table table = dynamoDB.getTable(TABLE_NAME);

		Item productItem = new Item()
				.withPrimaryKey("productId", product.getProductId())
				.withString("description", product.getDescription())
				.withString("imageUrl", product.getImageUrl())
				.withDouble("price", product.getPrice());
		
		try {
			PutItemOutcome outcome = table.putItem(productItem);
			result = true;
		} catch (AmazonDynamoDBException ex) {
			// Failed - TODO: Log an error
		}

		return result;
	}

	/**
	 * Insert a product (Asynchronously)
	 */
	public Future<PutItemResult> insertAsync(Product product) {
		Future<PutItemResult> result = new CompletableFuture<>();

		Map<String, AttributeValue> attributes = new HashMap<>();
		attributes.put("productId", ItemUtils.toAttributeValue(product.getProductId()));
		attributes.put("description", ItemUtils.toAttributeValue(product.getDescription()));
		attributes.put("imageUrl", ItemUtils.toAttributeValue(product.getImageUrl()));
		attributes.put("price", ItemUtils.toAttributeValue(product.getPrice()));

		try {
			result = dynamoClient.putItemAsync(TABLE_NAME, attributes);
		} catch (AmazonDynamoDBException ex) {
			// Failed - TODO: Log an error
			result.isDone();
		}

		return result;
	}

}
